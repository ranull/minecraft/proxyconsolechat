package com.ranull.proxyconsolechat.bukkit.listener;

import com.ranull.proxyconsolechat.bukkit.ProxyConsoleChat;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class AsyncPlayerChatListener implements Listener {
    private final ProxyConsoleChat plugin;

    public AsyncPlayerChatListener(ProxyConsoleChat plugin) {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onAsyncPlayerChat(AsyncPlayerChatEvent event) {
        if (plugin.getServer().getOnlinePlayers().size() == event.getRecipients().size()) {
            plugin.getChatManager().sendChatData(event.getPlayer(), event.getMessage());
        }
    }
}
