package com.ranull.proxyconsolechat.bukkit.manager;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.ranull.proxyconsolechat.bukkit.ProxyConsoleChat;
import org.bukkit.entity.Player;

public class ChatManager {
    private final ProxyConsoleChat plugin;

    public ChatManager(ProxyConsoleChat plugin) {
        this.plugin = plugin;
    }

    @SuppressWarnings("UnstableApiUsage")
    public void sendChatData(Player player, String message) {
        ByteArrayDataOutput byteArrayDataOutput = ByteStreams.newDataOutput();

        byteArrayDataOutput.writeUTF("ProxyConsoleChat");
        byteArrayDataOutput.writeUTF("Message");
        byteArrayDataOutput.writeUTF(player.getUniqueId().toString());
        byteArrayDataOutput.writeUTF(message);

        player.sendPluginMessage(plugin, "BungeeCord", byteArrayDataOutput.toByteArray());
    }
}
