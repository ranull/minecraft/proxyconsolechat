package com.ranull.proxyconsolechat.bungee.listener;

import com.ranull.proxyconsolechat.bungee.ProxyConsoleChat;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.connection.Server;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.UUID;

public class PluginMessageListener implements Listener {
    private final ProxyConsoleChat plugin;

    public PluginMessageListener(ProxyConsoleChat plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPluginMessage(PluginMessageEvent event) throws IOException {
        if (event.getTag().equals("BungeeCord") && event.getSender() instanceof Server) {
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(event.getData()));

            if (dataInputStream.readUTF().equals("ProxyConsoleChat") && dataInputStream.readUTF().equals("Message")) {
                try {
                    ProxiedPlayer proxiedPlayer = plugin.getProxy()
                            .getPlayer(UUID.fromString(dataInputStream.readUTF()));

                    plugin.getProxy().getLogger().info("[" + proxiedPlayer.getServer().getInfo().getName() + "] "
                            + proxiedPlayer.getName() + ": " + dataInputStream.readUTF());
                } catch (IllegalArgumentException ignored) {
                }
            }
        }
    }
}
