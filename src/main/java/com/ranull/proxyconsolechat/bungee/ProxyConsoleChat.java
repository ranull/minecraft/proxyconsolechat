package com.ranull.proxyconsolechat.bungee;

import com.ranull.proxyconsolechat.bungee.listener.PluginMessageListener;
import net.md_5.bungee.api.plugin.Plugin;

public class ProxyConsoleChat extends Plugin {
    @Override
    public void onEnable() {
        getProxy().getPluginManager().registerListener(this, new PluginMessageListener(this));
    }
}
